#!/bin/bash

# Used to be a simple build-success counting script, but now used to diagnose errors as well

# Our strategy: We want to go through each individual build results folder (that mock produced), and 
# see if there are RPMs in there.  If there are not, then the build failed.  In that case,
# we want to search for "No matching package" errors and figure out what we're missing to succeed.

# The idea is to take the output of this and copy/paste directly to the Wiki in markdown table.

# -Skip Grube, 2020-12-20


# Loop through folders in the current directory
# They are assumed to contain the build results, and be named according to package
for folder in `ls -1`; do
  

  # If there are RPM files in the build results directory, then the build is assumed to have succeeded, and we check the 
  # next folder for results
  rpmcount=`ls $folder/*.rpm 2> /dev/null   |   wc -l`
  
  if [[ "$rpmcount" != "0" ]]; then
    continue
  fi
	  
  

  # This long string of commands looks for the tell-tale "No matching package" error(s).
  # If found, we isolate the name(s) of the missing packages, make sure we don't repeat them, and store them in our REASON variable
  # (There is a hanging "\n" that gets swapped with a ",".  Too lazy to fix it for now)

  _tmp=`grep "No matching package to install" $folder/root.log`
  _tmp=`echo "$_tmp" | awk -F "install:" '{print $2}' | tr -d "\'" | sort | uniq | tr '\n' ','`
 
  REASON="$_tmp"
  

  # If reason is just ",", then we didn't find any missing deps, and the build failed for some other reason
  if [[ "$REASON" == "," ]]; then
    REASON="(unsure of failure, needs investigation)"
  else
    REASON="Missing dependency: ${REASON}"
  fi 
 

  # Output the package name and failure reason for easy markdown copy-paste to the Wiki
  echo "| $folder | $REASON |"
 
  

done
