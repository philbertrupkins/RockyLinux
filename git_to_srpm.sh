#!/bin/bash

# Retrieve the contents of a Rocky Linux git repository and build a source RPM package from it

# Takes one argument: a remote Git repo containing Rocky SRPM contents 
# (ex: git@gitlab.com:SkipGrube/rockyrpms/nginx.git )

# This script assumes:
# -mock build tool is installed, and this user is in the mock group
# -passwordless/key access to the remote git repo specified
# -git, tar, and other common core commands (mkdir, etc.) are available


# Order of operations:
# 
# 1. Clone git repository
# 2. compress upstream source code inside SOURCES/ back into tar.gz files
# 3. copy specs and SOURCES/ into temporary directory
# 4. call mock build tool to produce an SRPM


GITREPO="${1}"

if [ -z "${GITREPO}" ]; then
  echo "Error, remote GIT repo must be the first argument."
  exit 1
fi


###### Step 1: clone git repo (and record project name)

# Extract name of project by pulling end of gitrepo URL
# Spectool might do this more reliably
NAME=`echo "${GITREPO}" | awk -F '/' '{print $NF}' | sed 's/\.git//'`

# Clone it
rm -rf ${NAME}
git clone ${GITREPO}



###### Step 2: Compress upstream sources folder(s) back into tarballs

cd ${NAME}/SOURCES

# .rockymeta file contains list of tarballs that were part of the original upstream SRPM
for i in `cat .rockymeta`; do
  
  # We get the proper folder name by the name of the tar minus the filename extensions
  folder=`echo "${i}"   |   awk -F '\.tar' '{print $1}'`

  # Right now just assume .tar.gz.  Obviously will need to support the different compressions
  # (.tar.bz2, .tar.xz, .tar.Z , etc.)
  tar -cvzf  ${i}  ${folder}
  rm -rf ${folder}
done


# Back to CWD, outside of git root
cd ../../



##### Step 3: copy SPECS + SOURCES into temporary location
# temporary build location
rm -rf tmp
mkdir tmp

# Get sources (including tarballs) into temporary build location
cp -rf ${NAME}/SOURCES/* tmp/
cp -rf ${NAME}/SPECS/*  tmp/



##### Step 4: Build SRPM  (via mock)
rm -rf srpm
mkdir -p srpm

# ID .spec file for project (should be only 1):
specfile=`ls -1 tmp/*.spec | head -1`

# Actually build:
mock --buildsrpm --sources tmp/  --spec ${specfile} --resultdir=./srpm

echo "COMPLETE ::  SRPM for ${NAME} is located in ./srpm/ "

exit 0

# Further code: copy SRPM off somewhere?


