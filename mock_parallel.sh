#!/bin/bash


# Short and dirty parallel build script
# takes lists of srpms as input

# Ex: 
# ./mock_parallel.sh  srpmlist1  ~/srpmlist2  ~/list3


# Reads these lists and fires up mock in parallel - 1 thread per list
# You can build an arbitrary number of packages in parallel using this method - utilizing all processor cores

# I intend to do a bootstrap/test build of CentOS (and later RHEL)
# -Skip G.


# Generate RPM lists beforehand: (example)
# find /home/skip/build/sources/BaseOS-source/ -iname *.rpm | sort
#
# Skip's Strategy: 4 lists: BaseOS, Appstream1, Appstream2, Appstream3 (Each list ~600 packages)



count=0

# Loop through each argument which should be a big list of SRPMs
for file in `echo "$@"`; do
  
  # Keep track of which build thread we're starting, which we can label mock with "--uniqueext"
  let count=$count+1
  export count=${count}
  
  # Loop through the SRPMs in a given list, and have mock build them in turn
  # (Do it in a background sub-shell so we can start more threads of this)
  (
  for srpm in `cat "${file}"`; do
    _name=`echo $srpm  |  sed 's/\.src\.rpm//'`

    # We want a folder labeled with our SRPM list name (ex. "AppStream1"), and a subfolder with the RPM name
    # (to keep it organized!)
    _name=`basename $_name`
    folder=`basename $file`
    mkdir -p ~/build/build_space/$folder
  

  # Do the actual build.  Label this thread of mock with our unique number, and put the build contents in a properly labeled folder
  time mock -v -r /etc/mock/epel-8-x86_64.cfg  --uniqueext=${count}  --resultdir=~/build/build_space/$folder/$_name/  $srpm
  done
  ) &

done

# This script ends when all the mock commands finished - could be a while to build the whole distro(!)
wait
